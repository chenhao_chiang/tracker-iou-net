# Tracker-IoU-Net

This is the implementation of the Tracker-IoU-Net proposed in the Master's thesis "Speeding up the Generation of Image-Label Pairs from Videos Using Machine Learning" by Chen-Hao Chiang. The thesis was completed at TUM Department of Informatics. The semi-automatic annotation tool can be found in this repository: https://bitbucket.org/chenhao_chiang/annotataion_tracker-iou-net

The implementation of Tracker-IoU-Net is based on the tensorflow implementation of the GOTURN tracker. For the original codes, please visit the original author tangyuhao's github page:
https://github.com/tangyuhao/GOTURN-Tensorflow

The original GOTURN paper is: 

**[Learning to Track at 100 FPS with Deep Regression Networks](http://davheld.github.io/GOTURN/GOTURN.html)**,

[David Held](http://davheld.github.io/),[Sebastian Thrun](http://robots.stanford.edu/),[Silvio Savarese](http://cvgl.stanford.edu/silvio/)

## Tracker-IoU-Net Model:
1. Download the pre-trained model at: https://drive.google.com/open?id=1NGmPCZNU5NXRNxGNi8IEyBgPnz9ZLJry
2. Place the checkpoint files under the "checkpoint" folder.

## First Time Setup:
Here are the steps for setting up the environment on MacOS.

1. Install python 

        brew install python


2. Clone the repository
    
        git clone https://bitbucket.org/chenhao_chiang/tracker-iou-net/
        cd Tracker-IoU-Net
    

3. Download pip and create a virtual environment

        sudo easy_install pip
        sudo pip install virtualenv
        virtualenv [your_env] -p python3.6
    

4. Activate the virtual environment and install the requirements

        source [your_env]/bin/activate
        pip install -r requirements.txt
   

5. Leave the virtual environment when you're finished

        deactivate
  
## Requirements:
1. Python 3.6+.
2. [Tensorflow](https://www.tensorflow.org/) and its requirements. 
3. [NumPy](http://www.numpy.org/). 
4. [CUDA (Strongly Recommended)](https://developer.nvidia.com/cuda-downloads).

## Training
### Finetune on your own dataset
1. Place the target images in "train/target"
2. Place the search images in "train/search"
3. Create labels_train.txt in "train"
    - Each line contains the path (relative to "train") of the target image, search image, bounding box of search image (relative to the target image) and ground-truth IoU
    - Bounding box is randomly drawn from a laplace distribution
    - Bounding box is in the form of `<minX, minY, maxX, maxY>`
    - Example of one line: 
    `target/000024.jpg,search/000024.jpg,0.29269230769230764,0.22233115468409587,0.7991794871794871,0.7608061002178649, 0.8910234
    - To see the example code for creating training data from the ALOV300++ dataset, check "TrackerIoUNet/createTrainingData.py" in this repository: https://bitbucket.org/chenhao_chiang/annotataion_tracker-iou-net 
4. Create labels_val.txt in "train" as in 3.
5. Train the network

        python train.py

6. The log file is `train.log` by default
7. You can look at the graphs on tensorboard:

        tensorboard --logdir=train_summary

## Testing 
1. Test data can be generated in the same way as training data.
2. Place the target images in “test/target” and search images in “text/search”.
3. Go to the "test" folder and run `test.py` 

        python test.py

4. Check the IoU predictions in the log file. The log file is `test.log` by default

## License 
The Tracker-IoU-Net is released under the MIT License.
