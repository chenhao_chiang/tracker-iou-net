import logging
import time
import tensorflow as tf
import os
import numpy as np
import sys

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(basedir, os.path.pardir)))

from network import TrackerIoUNet

BATCH_SIZE = 10
WIDTH = 227
HEIGHT = 227

logfile = "test.log"
test_txt = "labels_test.txt"

def load_train_test_set(train_file):
    '''
    return train_set or test_set
    example line in the file:
    <target_image_path>,<search_image_path>,<x1>,<y1>,<x2>,<y2>
    (<x1>,<y1>,<x2>,<y2> all relative to search image)
    '''
    ftrain = open(train_file, "r")
    trainlines = ftrain.read().splitlines()
    train_target = []
    train_search = []
    train_box = []
    label = []
    for index, line in enumerate(trainlines):
        line = line.split(",")
        train_target.append(line[0])
        train_search.append(line[1])
        box = [10*float(line[2]), 10*float(line[3]), 10*float(line[4]), 10*float(line[5])]
        label.append([10*float(line[6])])
        train_box.append(box)
        # if index == 200:
        #     break
        
    ftrain.close()
    print("len:%d"%(len(train_target)))
    
    return [train_target, train_search, train_box, label]

def data_reader(input_queue):
    '''
    this function only reads the image from the queue
    '''
    search_img = tf.read_file(input_queue[0])
    target_img = tf.read_file(input_queue[1])

    search_tensor = tf.to_float(tf.image.decode_jpeg(search_img, channels = 3))
    search_tensor = tf.image.resize_images(search_tensor,[HEIGHT,WIDTH],
                            method=tf.image.ResizeMethod.BILINEAR)
    target_tensor = tf.to_float(tf.image.decode_jpeg(target_img, channels = 3))
    target_tensor = tf.image.resize_images(target_tensor,[HEIGHT,WIDTH],
                            method=tf.image.ResizeMethod.BILINEAR)
    box_tensor = input_queue[2]
    label_tensor = input_queue[3]

    return [search_tensor, target_tensor, box_tensor, label_tensor]


def next_batch(input_queue):
    min_queue_examples = 128
    num_threads = 1
    [search_tensor, target_tensor, box_tensor, label_tensor] = data_reader(input_queue)
    [search_batch, target_batch, box_batch, label_batch] = tf.train.batch(
        [search_tensor, target_tensor, box_tensor, label_tensor],
        batch_size=BATCH_SIZE,
        num_threads=num_threads,
        capacity=min_queue_examples + (num_threads+2)*BATCH_SIZE)
    return [search_batch, target_batch, box_batch, label_batch]


if __name__ == "__main__":
    if (os.path.isfile(logfile)):
        os.remove(logfile)

    logging.basicConfig(format='%(message)s',
        level=logging.DEBUG,filename=logfile)

    [train_target, train_search, train_box, label] = load_train_test_set(test_txt)
    target_tensors = tf.convert_to_tensor(train_target, dtype=tf.string)
    search_tensors = tf.convert_to_tensor(train_search, dtype=tf.string)
    box_tensors = tf.convert_to_tensor(train_box, dtype=tf.float64)
    label_tensors = tf.convert_to_tensor(label, dtype=tf.float64)

    input_queue = tf.train.slice_input_producer([search_tensors, target_tensors, box_tensors, label_tensors],shuffle=False)
    batch_queue = next_batch(input_queue)
    trackerIOUNet = TrackerIoUNet.TrackerIoUNet(BATCH_SIZE, train = False)
    trackerIOUNet.build()

    sess = tf.Session()
    init = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()
    sess.run(init)
    sess.run(init_local)

    coord = tf.train.Coordinator()
    # start the threads
    tf.train.start_queue_runners(sess=sess, coord=coord)

    ckpt_dir = "../checkpoint"
    if not os.path.exists(ckpt_dir):
        os.makedirs(ckpt_dir)
    ckpt = tf.train.get_checkpoint_state(ckpt_dir)
    if ckpt and ckpt.model_checkpoint_path:
        saver = tf.train.Saver()
        saver.restore(sess, ckpt.model_checkpoint_path)
    
    differences = []

    try:
        for i in range(0, int(len(train_box)/BATCH_SIZE)):
            cur_batch = sess.run(batch_queue)
            start_time = time.time()
            [batch_loss, fc8] = sess.run([trackerIOUNet.loss, trackerIOUNet.fc8],feed_dict={trackerIOUNet.image:cur_batch[0],
                    trackerIOUNet.target:cur_batch[1], trackerIOUNet.bbox:cur_batch[2], trackerIOUNet.label:cur_batch[3],trackerIOUNet.train_placeholder:False})
            for index, row in enumerate(fc8):
                difference = 0.1*(float(row)-float(cur_batch[3][index]))
                logString = "Pred: " + str(0.1*row) + " Truth: " + str(0.1*cur_batch[3][index]) + " Difference: " + str(difference)
                logging.info(logString)
                differences.append(abs(difference))

            #ts = fc4.tostring()
            #s = np.fromstring(ts,dtype=int)

            #f.write(s)

    except KeyboardInterrupt:
        print("get keyboard interrupt")

    average_difference = sum(differences)/float(len(differences))
    logging.info("Average difference (absolute value: " + str(average_difference))


