# train file

import logging
import time
import tensorflow as tf
import os
import shutil
import sys

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(basedir, os.path.pardir)))

from network import TrackerIoUNet

NUM_EPOCHS = 100
BATCH_SIZE = 64
WIDTH = 227
HEIGHT = 227
train_txt = "labels_train.txt"
val_txt = "labels_val.txt"
logfile = "train.log"
trainLossLogFile = "train_loss.txt"
valLossLogFile = "val_loss.txt"

def load_training_set(train_file):
    '''
    return train_set
    '''
    ftrain = open(train_file, "r")
    trainlines = ftrain.read().splitlines()
    train_target = []
    train_search = []
    bbox = []

    label = []
    for index,line in enumerate(trainlines):
        line = line.split(",")
        train_target.append(line[0])
        train_search.append(line[1])
        box = [10 * float(line[2]), 10 * float(line[3]), 10 * float(line[4]), 10 * float(line[5])]
        bbox.append(box)
        label.append([10*float(line[6])])
    ftrain.close()

    return [train_target, train_search, bbox, label]


def data_reader(input_queue):
    '''
    this function only read the one pair of images and from the queue
    '''
    search_img = tf.read_file(input_queue[0])
    target_img = tf.read_file(input_queue[1])
    search_tensor = tf.to_float(tf.image.decode_jpeg(search_img, channels=3))
    search_tensor = tf.image.resize_images(search_tensor, [HEIGHT, WIDTH],
                                           method=tf.image.ResizeMethod.BILINEAR)
    target_tensor = tf.to_float(tf.image.decode_jpeg(target_img, channels=3))
    target_tensor = tf.image.resize_images(target_tensor, [HEIGHT, WIDTH],
                                           method=tf.image.ResizeMethod.BILINEAR)
    bbox_tensor = input_queue[2]
    label_tensor = input_queue[3]
    return [search_tensor, target_tensor, bbox_tensor, label_tensor]


def next_batch(input_queue):
    min_queue_examples = 128
    num_threads = 1
    [search_tensor, target_tensor, bbox_tensor, label_tensor] = data_reader(input_queue)
    [search_batch, target_batch, bbox_batch, label_batch] = tf.train.batch(
        [search_tensor, target_tensor, bbox_tensor, label_tensor],
        batch_size=BATCH_SIZE,
        num_threads=num_threads,
        capacity=min_queue_examples + (num_threads + 2) * BATCH_SIZE)
    return [search_batch, target_batch, bbox_batch, label_batch]

def next_random_batch(input_queue):
    min_queue_examples = 128
    num_threads = 1
    [search_tensor, target_tensor, bbox_tensor, label_tensor] = data_reader(input_queue)
    [search_batch, target_batch, bbox_batch, label_batch] = tf.train.shuffle_batch(
        [search_tensor, target_tensor, bbox_tensor, label_tensor],
        batch_size=BATCH_SIZE,
        num_threads=num_threads,
        capacity=min_queue_examples + (num_threads + 2) * BATCH_SIZE,
        seed=88,
        min_after_dequeue=min_queue_examples)
    return [search_batch, target_batch, bbox_batch, label_batch]

def get_vars_by_name(names):
    return_vars = []
    for v in tf.global_variables():
        #print(v)
        if v.name.split('/')[0] in names:
            continue
        if (len(v.name.split('/')) > 1) and (v.name.split('/')[1] in names):
            continue
        if (len(v.name.split('/')) > 1) and (v.name.split('/')[1].split(':')[0] in names):
            continue
        
        return_vars.append(v)

    return return_vars

if __name__ == "__main__":
    if (os.path.isfile(logfile)):
        os.remove(logfile)
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG, filename=logfile)

    [train_target, train_search, train_bbox, train_label] = load_training_set(train_txt)
    target_tensors = tf.convert_to_tensor(train_target, dtype=tf.string)
    search_tensors = tf.convert_to_tensor(train_search, dtype=tf.string)
    bbox_tensors = tf.convert_to_tensor(train_bbox, dtype=tf.float64)
    label_tensors = tf.convert_to_tensor(train_label, dtype=tf.float64)
    input_queue = tf.train.slice_input_producer([search_tensors, target_tensors, bbox_tensors, label_tensors], shuffle=True)
    batch_queue = next_batch(input_queue)

    ### val
    [val_target, val_search, val_bbox, val_label] = load_training_set(val_txt)
    val_target_tensors = tf.convert_to_tensor(val_target, dtype=tf.string)
    val_search_tensors = tf.convert_to_tensor(val_search, dtype=tf.string)
    val_bbox_tensors = tf.convert_to_tensor(val_bbox, dtype=tf.float64)
    val_label_tensors = tf.convert_to_tensor(val_label, dtype=tf.float64)

    val_input_queue = tf.train.slice_input_producer([val_search_tensors, val_target_tensors, val_bbox_tensors, val_label_tensors], shuffle=True)
    val_batch_queue = next_random_batch(val_input_queue)
    #val_batch_queue = next_batch(val_input_queue)
    
    trackerIOUNet = TrackerIoUNet.TrackerIoUNet(BATCH_SIZE)
    trackerIOUNet.build()

    global_step = tf.Variable(0, trainable=False, name="global_step")

    train_op = tf.train.AdamOptimizer(0.001, 0.9)
    train_step = train_op.minimize( \
        trackerIOUNet.loss_wdecay, global_step=global_step)

    # plot gradients
    grads = train_op.compute_gradients(trackerIOUNet.loss_wdecay)

    # Add histograms for gradients.
    for grad, var in grads:
        if grad is not None:
            tf.summary.histogram(var.op.name + '/gradients', grad)

    merged_summary = tf.summary.merge_all()
    sess = tf.Session()
    
    init = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()
    sess.run(init)
    sess.run(init_local)

    coord = tf.train.Coordinator()
    # start the threads
    tf.train.start_queue_runners(sess=sess, coord=coord)

    ckpt_dir = "../checkpoint"
    # if not os.path.exists(ckpt_dir):
    #     os.makedirs(ckpt_dir)
    ckpt = tf.train.get_checkpoint_state(ckpt_dir)
    start = 0
    if ckpt and ckpt.model_checkpoint_path:
        start = int(ckpt.model_checkpoint_path.split("-")[1])
        logging.info("start by iteration: %d" % (start))
        if start == 1:
            print("Start by ",start)
            if os.path.exists('./train_summary'):
                shutil.rmtree('./train_summary') # delete the train_summary folder if training from the very beginning
            ftrainLossLog = open(trainLossLogFile, "w")
            ftrainLossLog.close()
            fvalLossLog = open(valLossLogFile, "w")
            fvalLossLog.close()
            saver = tf.train.Saver(var_list=get_vars_by_name(['fc4','fc5','fc6','fc7','fc8','scale','beta','pop_mean','pop_var']))
        else:
            print("Start by ",start)
            saver = tf.train.Saver()
        saver.restore(sess, ckpt.model_checkpoint_path)
    else:
        print("No checkpoint files found!!")
        exit()
    
    train_writer = tf.summary.FileWriter('./train_summary', sess.graph)
    assign_op = global_step.assign(start)
    sess.run(assign_op)
    model_saver = tf.train.Saver(max_to_keep=2)
    train_loss = []

    startEpoch = int(start / int(len(train_label) / BATCH_SIZE))
    logging.info("start epoch[%d]" % (startEpoch))
    print("start epoch[%d]" % (startEpoch))

    try:
        for i in range(start, int(len(train_label) / BATCH_SIZE * NUM_EPOCHS)):
            if i % int(len(train_label) / BATCH_SIZE) == 0:
                epoch = int(i / int(len(train_label) / BATCH_SIZE))
                logging.info("start epoch[%d]" % (epoch))
                print("start epoch[%d]" % (epoch))
                if i > start:
                    save_ckpt = "checkpoint.ckpt"
                    last_save_itr = i
                    model_saver.save(sess, ckpt_dir +  "/" + save_ckpt, global_step=i + 1)
                
                    # calculate and log validation loss
                    val_cur_batch = sess.run(val_batch_queue)
                    val_loss = []
                    #print(len(train_loss))
                    for index in range(100):
                        #print(index)
                        [val_batch_loss, val_fc8] = sess.run([trackerIOUNet.loss, trackerIOUNet.fc8],feed_dict={trackerIOUNet.image:val_cur_batch[0],
                            trackerIOUNet.target:val_cur_batch[1], trackerIOUNet.bbox:val_cur_batch[2], trackerIOUNet.label:val_cur_batch[3], trackerIOUNet.train_placeholder:False})
                        val_loss.append(val_batch_loss / BATCH_SIZE)
                    
                    average_val_loss = sum(val_loss)/float(len(val_loss))
                    print("val_loss: ",average_val_loss)
                    tf.summary.scalar("Validation_loss", average_val_loss)
                    val_loss = []

                    fvalLossLog = open(valLossLogFile, "a")
                    if epoch == 1:
                        fvalLossLog.write("%.3f" % (average_val_loss))
                    else:
                        fvalLossLog.write(",%.3f" % (average_val_loss))
                    fvalLossLog.close()

                    # calculate and log training loss
                    average_train_loss = sum(train_loss)/float(len(train_loss))
                    print("train_loss: ",average_train_loss)
                    tf.summary.scalar("Training_loss", average_train_loss)
                    ftrainLossLog = open(trainLossLogFile, "a")
                    if epoch == 1:
                        ftrainLossLog.write("%.3f" % average_train_loss)
                    else:
                        ftrainLossLog.write(",%.3f" % average_train_loss)
                    ftrainLossLog.close()
                    train_loss = []

                print(global_step.eval(session=sess))

            cur_batch = sess.run(batch_queue)

            start_time = time.time()
            [_, loss] = sess.run([train_step, trackerIOUNet.loss], feed_dict={trackerIOUNet.image: cur_batch[0],
                                                                         trackerIOUNet.target: cur_batch[1],
                                                                         trackerIOUNet.bbox: cur_batch[2],
                                                                         trackerIOUNet.label: cur_batch[3],
                                                                         trackerIOUNet.train_placeholder:True })
            logging.debug(
                'Train: time elapsed: %.3fs, average_loss: %f' % (time.time() - start_time, loss / BATCH_SIZE))
            print("Average loss: ", loss / BATCH_SIZE)
            train_loss.append(loss / BATCH_SIZE)

            if i % 10 == 0 and i > start:
                summary = sess.run(merged_summary, feed_dict={trackerIOUNet.image: cur_batch[0],
                                                              trackerIOUNet.target: cur_batch[1],
                                                              trackerIOUNet.bbox: cur_batch[2],
                                                              trackerIOUNet.label: cur_batch[3],
                                                              trackerIOUNet.train_placeholder:True })
                train_writer.add_summary(summary, i)

    except KeyboardInterrupt:
        print("get keyboard interrupt")
        if (i - start > 1000):
            model_saver = tf.train.Saver()
            save_ckpt = "checkpoint.ckpt"
            model_saver.save(sess, ckpt_dir +  "/" + save_ckpt, global_step=i + 1)